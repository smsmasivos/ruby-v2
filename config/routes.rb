Rails.application.routes.draw do
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  get '/', to: 'index#index'
  get '/getCredits', to: 'function#getCredits'
  get '/sendMessage', to: 'function#sendMessage'
  

  get '/auth', to: 'auth#auth'
  get '/authRegister', to: 'function#authRegister'
  get '/authValidation', to: 'function#authValidation'
  
end
