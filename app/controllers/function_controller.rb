require 'net/http'
require 'uri'

class FunctionController < ApplicationController
    def getCredits
        uri = URI.parse("https://api.smsmasivos.com.mx/credits/consult")
        headers = {
            "apikey": params["api"]
        }
        request = Net::HTTP::Post.new(uri, headers)
        req_options = {
          use_ssl: uri.scheme == "https",
        }
        response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
          http.request(request)
        end
        
        respond_to do |format|
            format.html
            format.text {render json: response.body}
        end
    end


    def sendMessage
        uri = URI.parse("https://api.smsmasivos.com.mx/sms/send")
        headers = {
          "apikey": params["api"]
        }

        infoRequest = {
            "message": params["message"],
            "numbers": params["addressee"],
            "country_code": params["country"],
            "sandbox": params["sandbox"]
        }

        if !params["name"].blank?
            infoRequest["name"] = params["name"]
        end
        
        request = Net::HTTP::Post.new(uri, headers)
        request.set_form_data(
            infoRequest
        )
        req_options = {
        use_ssl: uri.scheme == "https",
        }
        response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
        http.request(request)
        end

        respond_to do |format|
            format.html
            format.text {render json: response.body}
        end


    end


    def authRegister

        uri = URI.parse("https://api.smsmasivos.com.mx/protected/"+params["formato"]+"/phones/verification/start")

        headers = {
            "apikey": params["api"]
        }

        infoRequest = {
            "phone_number" => params["addressee"],
            "country_code" => params["country"],
            "code_length" => params["digits"]
        }

        if !params["name"].blank?
            infoRequest["company"] = params["name"]
        end

        request = Net::HTTP::Post.new(uri, headers)
        request.set_form_data(
            infoRequest
        )
    
        req_options = {
            use_ssl: uri.scheme == "https",
        }
        response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
            http.request(request)
        end

        respond_to do |format|
            format.html
            format.text {render json: response.body}
        end

    end

    def authValidation

        uri = URI.parse("https://api.smsmasivos.com.mx/protected/"+params["formato"]+"/phones/verification/check")
        headers = {
            "apikey": params["api"]
        }
        request = Net::HTTP::Post.new(uri, headers)
        request.set_form_data(
            "phone_number" => params["dest"],
            "verification_code" => params["codigo"]
        )
        req_options = {
        use_ssl: uri.scheme == "https",
        }
        response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
            http.request(request)
        end

        respond_to do |format|
            format.html
            format.text {render json: response.body}
        end
    end

end
